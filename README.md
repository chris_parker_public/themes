# Themes

This project is intended to hold a few themes I plan.  I'm going to start with a GTK Nord theme based on Mint-Y-Dark-Blue.  Next steps are to add a light theme using the same color scheme, after which I plan on setting up SciTE and Geany themes based on the same colors.

This is a work in progress.